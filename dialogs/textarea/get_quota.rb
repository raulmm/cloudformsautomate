#
# Description: Method to display quota and other information.
#
# Author: Raul Mahiques <rmahique@redhat.com>
# License: GPL v3
#
begin
  @method = 'list_quota'
  @handle = $evm
  @debug = @handle.object['debug']
  @handle.log("info", "#{@method} - EVM Automate method started")
  @tenant = @handle.root['tenant']
  
  # Load libraries
  require 'json'
  
  # Define functions
  # To simplify debugging we create a simple function:
  def logInfo(msg)
    @handle.log('info', "#{@method}: " + msg.to_s) if @debug
  end

  # Get the value with some error handling (integers only).
  def getDialog(dialogName,part)
    item = ''
    logInfo("Dialog value for dialog \"#{dialogName}\": #{@handle.root[dialogName]}")
    if not @handle.root[dialogName].nil? and not @handle.root[dialogName].is_a? Fixnum
      item = "#{@handle.root[dialogName].split(/_:_/)[part.to_i]}"
    elsif not @handle.root[dialogName].nil? and @handle.root[dialogName].is_a? Fixnum
      item = "#{@handle.root[dialogName]}"
    end
    return nil => "<none>" if item.blank?
    item
  end
  
  ###############################################################################################
  # MAIN
  ###############################################################################################
  # populate variables
  @my={}
  ['cluster'].each do |i|
    @my[i] = getDialog('dialog_param_'+i,1)
  end
  ['storage','serv_name','sbu','stage','os','vms','vcpu','memory','extra_disk','cmdb_vlan'].each do |i|
    @my[i] = getDialog('dialog_param_'+i,0)
  end
  ['storage_default_size'].each do |i|
    @my[i] = getDialog(i,0)
  end
  userdept = nil
  @quota_max_memory = @quota_max_storage = @quota_max_vcpu = @quota_max_vms = @allocated_memory = @allocated_storage = @allocated_vcpu = @noquota = 0
  @miq_group = @handle.root['miq_group']
  myuser = @handle.object['user']

  @messageQ = "QUOTA:\n"

  # Check if the user has quota tags defined or not.
  if myuser.current_group.tags().nil? or myuser.current_group.tags().blank?
    @messageQ = "\tThis user (#{myuser.userid}) have no quotas."
    @noquota = 1
  else
    logInfo("group tags for user #{myuser.userid}: #{myuser.current_group.tags()}")
    # Lets get all the quotas for the user group.
    myuser.current_group.tags().each do |tag|
      cat=tag.split("/")[0]
      val=tag.split("/")[1] 
      if cat.include? "quota_max_"
        # Define the variables
        instance_variable_set("@" + cat, val.to_i)
        logInfo("messageQ += #{cat} = #{val}")
        # We need to get the description to display it in the dialog, otherwise we will use the raw value.
        @category = @handle.vmdb(:classification).find_by_name(cat)
        unless @category.nil?
          @category.entries.each do |tagdb|
            if tagdb.name == val
              logInfo("tagdb.description: #{tagdb.description} ")
              @messageQ += "\t#{@category.description}: #{tagdb.description}\n"
            end
          end
        else
          @messageQ += "\t#{cat}: #{val}\n"
          logInfo("No tag category found, this should never happen")
        end
      else
        logInfo("Not a quota max tag: #{cat} = #{val}")
      end
    end
  end
  logInfo("Total quotas: #{@messageQ}")
  # We have populated the messageQ with the values for the quota, now we popullate it with the current allocation values
  @messageQ += "ALLOCATION:\n"
  # And prepare the message that will show how many resources are left.
  @calculation = "REMAINING RESOURCES:\n"


 
  @vmName=nil
  # DIALOG: Get the server name
  unless @my['serv_name'][0].nil? or @my['serv_name'].blank? or @my['sbu'][0].nil? or @my['sbu'].blank? or @my['stage'][0].nil? or @my['stage'].blank? or @my['os'][0].nil? or @my['os'].blank?
    @vmName = "\tServer Names:\t#{@my['os']}-#{@my['serv_name']}#{@my['stage']}00..#{@my['vms'].to_s.rjust(3, '0')}.example.com"
    logInfo("myVmname: #{@my['serv_name']}")
  end
  
  @totSize=nil
  # DIALOG: Get the total size
  unless @my['vms'][0].nil? or @my['vms'].blank? or @my['vcpu'][0].nil? or @my['vcpu'].blank? or @my['memory'][0].nil? or @my['memory'].blank?
    @totCPU = @my['vcpu'].to_i * @my['vms'].to_i
    @totRam = @my['memory'].to_i * @my['vms'].to_i
    @totStorage = ( @my['storage_default_size'].to_i + @my['extra_disk'].to_i ) * @my['vms'].to_i
    @totSize="\tTotal resources:\tCPUs: #{@totCPU}, RAM: #{@totRam}, Storage: #{@totStorage} GB"
  else
    logInfo("myNum_instances: #{@my['vms']} - myCpus: #{@my['vcpu']} - myRam: #{@my['memory']}")
  end



  # We need to get the information about the current allocation.
  # With this information we can calculate how much is available.
  @miq_group.virtual_column_names.each do |virtual_column_name| 
    case
    when virtual_column_name == "allocated_memory"
      @allocated_memory = @miq_group.send(virtual_column_name).to_i
      unless @quota_max_memory == 0 or @allocated_memory.nil?
        # quota is in GB, we need to convert first to bytes
        @valueQuota = ( @quota_max_memory.to_i * 1048576 ) - @allocated_memory.to_i - ( @totRam * 1073741824)
        @calculation += "OVERQUOTA" if @valueQuota <= 0
        @calculation += "\tmemory: #{@valueQuota / 1024 / 1024 / 1024} GB\n"
      end
    when virtual_column_name == "allocated_storage"
      logInfo("message 4") 
      @allocated_storage = @miq_group.send(virtual_column_name).to_i
      unless @quota_max_storage == 0 or @allocated_storage.nil?
        logInfo("message 5 quota storage: #{@quota_max_storage.to_i} allocated storage: #{@allocated_storage}")
        # quota is in GB, we need to convert first to bytes
        @valueQuota = (@quota_max_storage.to_i  * 1073741824) - @allocated_storage.to_i - ( @totStorage * 1073741824)
        @calculation += "OVERQUOTA" if @valueQuota <= 0
        @calculation += "\tstorage: #{@valueQuota / 1024 / 1024 / 1024} GB\n"
      end
      logInfo("message 5b")
    when virtual_column_name == "allocated_vcpu"
      logInfo("message 6")
      @allocated_vcpu = @miq_group.send(virtual_column_name).to_i
      unless @quota_max_vcpu == 0 or @allocated_vcpu.nil?
        logInfo("message 7")
        @valueQuota = @quota_max_vcpu.to_i - @allocated_vcpu.to_i - @totCPU
        @calculation += "OVERQUOTA" if @valueQuota <= 0
        @calculation += "\tvcpu: #{@valueQuota}\n"
      end
    else
      logInfo("no allocated resources #{virtual_column_name}!!!!!")
    end
    logInfo("\t#{virtual_column_name} = #{@miq_group.send(virtual_column_name)}")
  end

  # Collect info about the networks that will be used  
  @networks=''
  if @my['cmdb_vlan'] == '' or @my['cmdb_vlan'].nil? or @my['cmdb_vlan'][0].nil? or @my['cmdb_vlan'] == "no_network_selected"
    @networks += "\n\tNo network selected\n"
  else
    @networks="\n\tNetworks:\n"
    logInfo(@my['cmdb_vlan'])
    JSON.parse(@my['cmdb_vlan']).each do |key,val|
      if val.is_a? Hash
        @networks += "\t\t#{key}:\n\t\t"
        val.each do |k,v|
          if k.upcase == "VLAN"
            @networks += "#{k.upcase}: #{v.to_s.ljust(6, ' ')}"
          elsif k.upcase == "NET"
            @networks += "#{k.upcase}: #{v.to_s.ljust(20, ' ')}"
          elsif k.upcase == "ZONE"
            @networks += "#{k.upcase}: #{v.to_s.ljust(23, ' ')}"
          else
            @networks += "#{k.upcase}: #{v.to_s.ljust(28, ' ')}"
          end
        end
        @networks += "\n"
      else
        @networks += "\t\t#{key}:"
        @networks += "#{val.to_s.rjust(23)}\n"
      end
    end
  end


  # Get the free space
  unless @my['storage'][0].nil? or @my['storage'].blank?
    logInfo("mycluster: #{@my['cluster']} , myStorage: #{@my['storage']}")
    @tenant.ext_management_systems.each do |p|
      p.ems_clusters.each do |c|
        if c.uid_ems =~ /#{@my['cluster']}/
          logInfo("Selected #{@my['cluster']}")
          c.storages.each do |s|
            if s['id'].to_i == @my['storage'].to_i
              logInfo("Selected #{@my['storage']}")
              @free_space = s['free_space']
            else
              logInfo("#{s['id']} - #{@my['storage']}")
            end
          end
        else
          logInfo("#{c['uid_ems']} - #{@my['cluster']}")
        end
      end
    end
    @space = "\n\tDisk space available:   #{@free_space / 1024 / 1024 / 1024} GB" unless @free_space.nil?
  end
 
  # Show how much is needed
  if @noquota == 0
    @messageQ += "\tAllocated memory: #{@allocated_memory / 1024 / 1024 / 1024 }GB
\tAllocated Storage: #{@allocated_storage / 1024 / 1024 / 1024 }GB
\tAllocated CPU cores: #{@allocated_vcpu}
REQUEST:
#{@space}#{@vmName}
#{@totSize}#{@space}#{@networks}"
  else
    @calculation = ""
    @messageQ = "REQUEST:
#{@vmName}
#{@totSize}#{@space}#{@networks}
QUOTAS: No quotas assigned, no resource restrictions"
  end
  
  @list_values = {
    'required'  => true,
    'protected' => false,
    'read_only' => true,
    'value'     => @messageQ + "\n" + @calculation
  }
  @list_values.each do |key, value|
    @handle.object[key] = value
  end
  logInfo(" Dialog message: <#{@list_values}>")
  exit MIQ_OK
rescue => err
  @handle.log("error", "#{@method} - [#{err}]\n #{err.backtrace.join("\n")}")
  exit MIQ_ABORT
end

